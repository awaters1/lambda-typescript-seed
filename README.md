# lambda-typescript-seed

AWS Lambda Serverless TypeScript seed project with the following features

1. Serverless deployment
2. Offline development for Lambda, API Gateway and DynamoDB
3. TypeScript support through WebPack


# Development Setup
A development environment setup with [NodeJS](https://nodejs.org/en/) through [NVM](https://github.com/nvm-sh/nvm), [Yarn](https://yarnpkg.com/en/) and [Serverless](https://serverless.com/).

1. `yarn install`
1. `serverless dynamodb install`

# Running Locally
`serverless offline start`

Will start a local DynamoDB instance on port 8001 and a local API Gateway instance on port 3005, be sure to add add a `x-api-key` header, the value will be printed in the terminal

To check the local DynamoDB table you can run the command `
aws dynamodb scan --table-name lambda-typescript-seed-dev --endpoint-url http://localhost:8001`

# Deploying

`serverless deploy`
