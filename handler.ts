import { APIGatewayProxyHandler } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import 'source-map-support/register';

const TABLE_NAME = process.env.DYNAMODB_TABLE;
const EXPIRES = 36000 // 10 hours

let docClient: AWS.DynamoDB.DocumentClient;
if (process.env.IS_OFFLINE) {
  docClient = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8001',
    accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
    secretAccessKey: 'DEFAULT_SECRET' // needed if you don't have aws credentials at all in env
  })
} else {
  docClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});
}

interface ParsePostBody {
  ipAddress: string;
}

export const parse: APIGatewayProxyHandler = async (event, _context) => {
  const body = event.body;
  const bodyObj = JSON.parse(body) as ParsePostBody;
  const params = {
    TableName: TABLE_NAME,
    Key: {
      sourceIp: bodyObj.ipAddress,
    }
  }
  return docClient.get(params).promise()
    .then((data) => {
      if (data.Item) {
        return {
          statusCode: 200,
          body: JSON.stringify({
            message: "Found IP Address"
          })
        };
      }
      return {
        statusCode: 404,
        body: JSON.stringify({
          message: "Missing IP Address"
        })
      };
    })
    .catch((err) => {
      console.error("Unable to get source from ip", err);
      return {
        statusCode: 500,
        body: JSON.stringify({
          message: "Error getting IP"
        })
      };
    });
}

interface RecordPostBody {
  source: string;
}

export const registerIp: APIGatewayProxyHandler = async (event, _context) => {
  const sourceIp = event.requestContext.identity.sourceIp;
  const body = event.body;
  const recordIp = JSON.parse(body) as RecordPostBody;
  const params = {
    TableName: TABLE_NAME,
    Item: {
      sourceIp,
      expires: Math.floor(new Date().getTime() / 1000) + EXPIRES,
      source: recordIp.source,
    }
  };
  
  return docClient.put(params).promise()
    .then(() => {
      return {
        statusCode: 200,
        body: JSON.stringify({}),
        headers: {
          'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        },
      };
    }).catch((err) => {
      console.error("Unable to record IP", err);
      return {
        statusCode: 500,
        body: JSON.stringify({}),
      };
    });
}

